package fr.esimed.catapi

import android.util.JsonReader
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class CatService(catDao:CatDAO) {

    val api_key ="a7471e3a-f530-4eed-8ca6-7beaac7173b9"
    val api_name = "x-api-key"
    //val db = CatDatabase.getDatabase(this@MainActivity).getDao()
    //val catService:CatService = CatService(db)
    val dao = catDao

    fun loadBreeds():Boolean{
        val url = URL("https://api.thecatapi.com/v1/breeds")
        var result = false
        val urlConnection = url.openConnection() as HttpsURLConnection
        try {
            urlConnection.setRequestProperty("x-api-key",api_key)
            urlConnection.connect()
            val reader = JsonReader(urlConnection.inputStream.bufferedReader())
            val int = 5
            reader.beginArray()

            var id: Int = 1
            var name: String = ""
            var origin: String = ""
            var breed_id: String = ""
            while (reader.hasNext()) {

                reader.beginObject()
                while (reader.hasNext()) {
                    val str = reader.nextName()
                    if (str.equals("id")) {
                        breed_id = reader.nextString()
                    } else if (str.equals("name")) {
                        name = reader.nextString()
                    } else if (str.equals("origin")) {
                        origin = reader.nextString()
                    } else {
                        reader.skipValue()
                    }

                }
                reader.endObject()
                val breed = Breed(id, breed_id, name, origin)
                dao.insert(breed)
                id++

            }
            reader.endArray()
            result = true
            //}
        } finally {
            urlConnection.disconnect()
        }
        return result
    }

    fun loadImages(breed:Breed):List<String>{

        val url_images = URL("https://api.thecatapi.com/v1/images/search?breed_ids=${breed.breed_id}&limit=10")
        val urlConnection = url_images.openConnection() as HttpsURLConnection
        val list_url: MutableList<String> = mutableListOf()
        urlConnection.setRequestProperty("x-api-key", api_key)
        urlConnection.connect()
        val reader = JsonReader(urlConnection.inputStream.bufferedReader())
        if (urlConnection.responseCode == 200) {

            reader.beginArray()
            var url_image = ""
            while (reader.hasNext()) {
                reader.beginObject()
                while (reader.hasNext()) {
                    val str = reader.nextName()
                    if (str.equals("url")) {
                        url_image = reader.nextString()
                    } else {
                        reader.skipValue()
                    }

                }
                reader.endObject()
                list_url.add(url_image)
            }
            reader.endArray()
        }
        var int = 0
        for (i in list_url) {
            println(i)
            int++
        }

        return list_url
    }
}