package fr.esimed.catapi

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context



@Database(entities=arrayOf(Breed::class), version=1)
abstract class CatDatabase : RoomDatabase() {


    companion object {
        var INSTANCE:CatDatabase?=null

        @JvmStatic
        fun getDatabase(context: Context): CatDatabase {
            if (INSTANCE == null) {
                this.INSTANCE = Room.databaseBuilder(context, CatDatabase::class.java,
                        "cats.db").allowMainThreadQueries().build()
            }
            return INSTANCE!!
        }
    }

    abstract fun getDao(): CatDAO
    //fun seed(){
        //val catDao:CatDAO = CatDAO()
        //if (catDao.getCount() === 0) {
           // val id = catDao.insert(Breed("Compte courant", 1225.25))

        //}
    //}
}