package fr.esimed.catapi
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query


@Dao
public interface CatDAO{
    @Query("SELECT * FROM Breed")
    fun getAllBreeds(): Array<Breed>

    @Query("SELECT * FROM Breed WHERE id=:id")
    fun getBreedById(id: Long): Breed

    @Query("SELECT count(*) FROM Breed")
    fun getCount(): Int

    @Query("DELETE FROM Breed")
    fun deleteAll()

    @Insert
    fun insert(breed: Breed)

    @Delete
    fun delete(breed: Breed)
}