package fr.esimed.catapi

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.card_image.*

class MainActivity : AppCompatActivity() {
    private lateinit var linearLayoutManager: LinearLayoutManager
    inner class LoadBreedsTask : AsyncTask<Void,Void,Boolean>(){
        val db = CatDatabase.getDatabase(this@MainActivity).getDao()
        val catService:CatService = CatService(db)
        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            progress_bar.visibility = View.GONE
            val list = db.getAllBreeds()
            if(result!!) {
                breeds_spinner.adapter = ArrayAdapter<Breed>(this@MainActivity,android.R.layout.simple_dropdown_item_1line,list)
            }
        }

        override fun doInBackground(vararg params: Void?): Boolean {
            var res = false
            if(catService.loadBreeds()){
                //val list = db.getAllBreeds()
                //list.forEach { println(it.breed_id) }
               res = true
            }
            return res
        }

        override fun onPreExecute() {
            super.onPreExecute()
            progress_bar.visibility = View.VISIBLE
        }
    }

    inner class LoadImagesListTask(val breed:Breed) : AsyncTask<Void,Void,List<String>>(){
        val db = CatDatabase.getDatabase(this@MainActivity).getDao()
        val catService:CatService = CatService(db)
        override fun onPostExecute(list: List<String>) {
            super.onPostExecute(list)
            progress_bar.visibility = View.GONE
            recycler_view.adapter = ImageAdapter(list)

        }

        override fun doInBackground(vararg params: Void?): List<String> {
            val list = catService.loadImages(breed)
            return list
        }

        override fun onPreExecute() {
            super.onPreExecute()
            progress_bar.visibility = View.VISIBLE
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        linearLayoutManager = LinearLayoutManager(this)
        recycler_view.layoutManager = linearLayoutManager

        val db = CatDatabase.getDatabase(this).getDao()
        val cat_service = CatService(db)
        val list = db.getAllBreeds()
        //db.deleteAll()
        if(list.isEmpty()){
          this.LoadBreedsTask().execute()
        } else {
            breeds_spinner.adapter = ArrayAdapter<Breed>(this@MainActivity,android.R.layout.simple_dropdown_item_1line,list)
        }
        breeds_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val breed = parent?.getItemAtPosition(position) as Breed
                val list:List<String> = listOf()
                //recycler_view.adapter = adapter
                LoadImagesListTask(breed).execute()
                Toast.makeText(this@MainActivity, breed.name, Toast.LENGTH_SHORT).show()
                recycler_view.visibility=View.VISIBLE
                //toast("Breed id :${breed.id} , breed name :${breed.name}")
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }

    }



}

