package fr.esimed.catapi

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.ProgressBar

data class ImageViewHolder(val cardView:CardView,val imageView:ImageView, val progressBar: ProgressBar)
    : RecyclerView.ViewHolder(cardView){

}

