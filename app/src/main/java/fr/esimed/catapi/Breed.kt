package fr.esimed.catapi

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Breed (@PrimaryKey(autoGenerate = true)
    val id:Int,
    val breed_id:String,
    val name:String,
    val origin:String)
{
    override fun toString(): String {
        return name
    }
}
