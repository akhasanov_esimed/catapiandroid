package fr.esimed.catapi

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.card_image.view.*
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class ImageAdapter(val list_url:List<String>) : RecyclerView.Adapter<ImageViewHolder>(){
    val bitMap:Array<Bitmap?> = Array(list_url.size){null}
    //val db = CatDatabase.getDatabase(this@MainActivity).getDao()
    //val catService:CatService = CatService(db)

    inner class LoadImageTask(val imageView: ImageView,val position:Int) : AsyncTask<String, Void, Bitmap>() {
        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)
            //imageView.progress_bar_image.visibility = View.GONE
            //imageView.recycler_view.visibility=View.VISIBLE
            if(result!=null){
                imageView.setImageBitmap(result)
                imageView.visibility=View.VISIBLE
            }else{
                //Toast.makeText(imageView.context,"Error downloading",Toast.LENGTH_SHORT).show()
            }
        }

        override fun doInBackground(vararg urls: String): Bitmap? {
            val urlOfImage = list_url[position]
            try {
                val connection = URL(urlOfImage).openConnection() as HttpsURLConnection
                connection.doInput=true
                connection.connect()
                val inputStream = connection.inputStream
                val bitmap = BitmapFactory.decodeStream(inputStream)
                bitMap[position] = bitmap
                return bitmap
            } catch (e: Exception) { // Catch the download exception
                e.printStackTrace()
                return null
            }
        }

        override fun onPreExecute() {
            super.onPreExecute()
            //imageView.progress_bar_image.visibility = View.VISIBLE
        }
    }

    override fun onBindViewHolder(imageHolder: ImageViewHolder, p1: Int) {
        val cardImage = imageHolder.imageView
        //this.LoadImageTask(cardImage,p1).execute()
        if(bitMap[p1] == null){
            this.LoadImageTask(cardImage,p1).execute()
        }
        cardImage.setImageBitmap(bitMap[p1])
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ImageViewHolder {
        val card_image =  LayoutInflater.from(parent.context).inflate(R.layout.card_image, parent, false)
        val imageHolder =  ImageViewHolder(card_image as CardView,card_image.findViewById(R.id.image_view),card_image.findViewById(R.id.progress_bar_image))
        return imageHolder
    }

    override fun getItemCount(): Int {
        return list_url.size
    }

}